#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 19 22:18:28 2021

@author: kantundpeterpan
"""

import os
import shlex
from subprocess import run, PIPE
from glob import glob

class pyPharmit():
    
    pharmit_exe = '/home/kantundpeterpan/Software/pharmit/pharmitserver'
    
    os.environ['LC_ALL'] = 'C'
    
    @classmethod
    def dbcreate(cls, sdf: str, dbdir: str, prefix: str = 'pharmit_db',
                 n_threads: int = os.cpu_count(),
                 split_by_threads = True, **kwargs):
        '''
        Parameters
        ----------
        cls : TYPE
            DESCRIPTION.
        sdf : str
            DESCRIPTION.
        dbdir : str
            DESCRIPTION.
        prefix : str, optional
            DESCRIPTION. The default is 'pharmit_db'.
        n_threads : int, optional
            DESCRIPTION. The default is os.cpu_count().
        split_by_threads : TYPE, optional
            DESCRIPTION. The default is True.
        **kwargs : TYPE
            DESCRIPTION.

        Returns
        -------
        res : TYPE
            DESCRIPTION.

        '''
        cmd = cls.pharmit_exe + ' -cmd dbcreate -in %s' % os.path.abspath(sdf)
        
        dbdir = os.path.abspath(dbdir)
        
        if split_by_threads:
            folders = []
            for i in range(n_threads):
                folders.append(os.path.join(dbdir, prefix + '_' + str(i)))
        
            cmd += ' '+' '.join(['-dbdir %s' % f for f in folders])
        
        for k,v in kwargs.items():
            cmd += ' -%s %s' % (k, v)
            
        res = run(shlex.split(cmd), stdout = PIPE, stderr = PIPE)
        
        return res
                    
    @classmethod
    def dbsearch(cls, pharma, dbdir: str, prefix: str = 'actives',
                 match_prefix = True,
                 **kwargs):
        cmd = cls.pharmit_exe + ' -cmd dbsearch'
        
        if isinstance(pharma, str):
            cmd += ' -in %s' % os.path.abspath(pharma)
        
        elif isinstance(pharma, list):
            tmp = []
            for p in pharma:
                tmp.append(os.path.abspath(p))
            
            cmd += ' '+' '.join(['-in %s' % p for p in tmp])
            
        if match_prefix:
            folders = glob(os.path.abspath(dbdir)+'/'+prefix+'*')
            cmd += ' '+' '.join(['-dbdir %s' % f for f in folders])
        else:
            cmd += ' '+' -dbdir %s' % os.path.abspath(dbdir)
            
        for k,v in kwargs.items():
            cmd += ' -%s %s' % (k, v)
            
        res = run(shlex.split(cmd), stdout = PIPE, stderr = PIPE)
        
        return res